<div class="panel panel-inverse overflow-hidden">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									    <i class="fa fa-plus-circle pull-right"></i> 
										Case File Details
									</a>
                        </h3>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="data-table" class="table table-striped table-bordered">
                                     <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Data</th>
                                         </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
